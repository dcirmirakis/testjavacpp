#ifndef MESSAGE_H_
#define MESSAGE_H_

#include <stdbool.h>

class A{
    private:
        int _i{0};
    public:
    A(int i) : _i(i) { }
};

class MessageClass{
    private:
        int _number{0};
        A _a{A(0)};
    public:
        int i1{0};
        MessageClass(int i); // { this->_number=i; }
        int getNumber() { return this->_number; }
        A getA() { return this->_a; }
};

#endif /* ndef MESSAGE_H_ */