import org.bytedeco.javacpp.*;
import org.bytedeco.javacpp.annotation.*;
import org.bytedeco.javacpp.tools.*;

@Properties(value=@Platform(
    include="/home/domin/dev/testjavacpp/MyLib/message.hpp",
    compiler = "cpp17",
    define = "UNIQUE_PTR_NAMESPACE std" ,
    linkpath = {"/home/domin/dev/testjavacpp/build/MyLib"},
    link="MyLib"/*, define={}*/), target="Message")

public class MessageConfig implements InfoMapper {
    public void map(InfoMap infoMap) {
        //infoMap.put(new Info("EXPORTS", "NOINLINE").cppTypes().annotations());
        //infoMap.put(new Info("!defined(DOXYGEN) && !defined(SWIG)").define(false));
        //infoMap.put(new Info("P_S_MESSAGE_STRUCT").valueTypes("S_MESSAGE_STRUCT"));
    }
}