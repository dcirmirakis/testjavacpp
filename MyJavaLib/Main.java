import org.bytedeco.javacpp.*;

public class Main {
    public static void main(String[] args) {
        try(PointerScope ps = new PointerScope()){
            Message.MessageClass my_class = new Message.MessageClass(10);
            System.out.println(my_class.getNumber());
            IntPointer i1 = new IntPointer(1).put(1);
            BytePointer c1 = new BytePointer(128);
            System.out.println(my_class.i1());
            Message.A a = new Message.A(10);
            Message.A b = my_class.getA();

            // my_struct.c1(c1);
            // //my_struct.len(c1.capacity());
            // Message.t_func(my_struct);
            System.out.println(a);
            System.out.println(b);
            System.out.println(my_class);
        
            System.out.println(ps.toString());
        }
    }
}