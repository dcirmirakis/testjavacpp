#! /bin/bash

g++ -std=c++14 -c -o message.o message.cpp
g++ -shared -o libmessage.so message.o

javac -cp javacpp.jar MessageConfig.java
java -jar javacpp.jar MessageConfig

javac -cp javacpp.jar  MessageConfig.java Message.java
java -jar javacpp.jar Message -nodelete
